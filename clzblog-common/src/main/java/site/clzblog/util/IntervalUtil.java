package site.clzblog.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class IntervalUtil {
    private static Date getDateByString(String time) {
        Date date = null;
        if (time == null) {
            return date;
        }

        String pattern = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        try {
            date = format.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getShortTime(String time) {
        String shortString = null;
        long now = Calendar.getInstance().getTimeInMillis();
        Date date = getDateByString(time);
        if (date == null) {
            return shortString;
        }
        long delTime = (now - date.getTime()) / 1000;
        int temp;
        if (delTime > 365 * 24 * 60 * 60) {

            temp = (int) (delTime / (365 * 24 * 60 * 60));
            shortString = temp > 1 ? temp + " years ago" : temp + " year ago";

        } else if (delTime > 24 * 60 * 60) {

            temp = (int) (delTime / (24 * 60 * 60));
            shortString = temp + " days ago";

        } else if (delTime > 60 * 60) {

            temp = (int) (delTime / (60 * 60));
            shortString = temp > 1 ? temp + " hours ago" : temp + " hour ago";

        } else if (delTime > 60) {

            temp = (int) (delTime / (60));
            shortString = temp > 1 ? temp + " minutes ago" : temp + " minute ago";

        } else if (delTime > 1) {

            shortString = delTime + " seconds ago";

        } else {

            shortString = "just now";

        }
        return shortString;
    }

    public static void main(String[] args) {
        String pattern = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        String s = format.format(new Date());
        System.out.println(getShortTime(s));
        System.out.println(getShortTime("2018-06-25 11:15:34"));
    }
}
