package site.clzblog.global.constant;

/**
 * @Desc Global Constant
 * @Author chengli.zou
 * @CreateDate 7/4/18
 */
public enum GlobalConstant {
    ANONYMOUS_USER("anonymousUser", 1),

    ONLINE("online", 2),

    OFFLINE("offline", 3),

    ROLE_ADMIN("ROLE_admin", 4),

    ROLE_USER("ROLE_user", 5);

    private String name;
    private int index;

    GlobalConstant(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
