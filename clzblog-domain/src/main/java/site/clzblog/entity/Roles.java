package site.clzblog.entity;

/**
 * @Desc Roles entity
 * @Author chengli.zou
 * @CreateDate 6/15/18
 */
public class Roles {
    private  Integer id;
    private String roleName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
