package site.clzblog.vo;

public class TagData {
    private Integer id;
    private String name;
    private String status;
    private String created;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public TagData() {
    }

    public TagData(Integer id, String name, String status, String created) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.created = created;
    }
}
