package site.clzblog.vo;

/**
 * @Desc
 * @Author chengli.zou
 * @Datetime 18-10-12 下午2:46
 **/
public class ResponseMessage {
    private String responseMessage;

    public String getResponseMessage() {
        return responseMessage;
    }

    public ResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
}
