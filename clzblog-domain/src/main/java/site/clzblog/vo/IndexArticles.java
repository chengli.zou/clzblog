package site.clzblog.vo;

public class IndexArticles {
    private Integer id;
    private String title;
    private int replies;
    private String date;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getReplies() {
        return replies;
    }

    public void setReplies(int replies) {
        this.replies = replies;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public IndexArticles() {
    }

    public IndexArticles(Integer id, String title, int replies, String date) {
        this.id = id;
        this.title = title;
        this.replies = replies;
        this.date = date;
    }
}
