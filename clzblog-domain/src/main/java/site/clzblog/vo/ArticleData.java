package site.clzblog.vo;

public class ArticleData {
    private Integer id;
    private String title;
    private String intro;
    private int[] tags_ids;
    private String[] tags_name;
    private byte[] body;
    private Integer state;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public int[] getTags_ids() {
        return tags_ids;
    }

    public void setTags_ids(int[] tags_ids) {
        this.tags_ids = tags_ids;
    }

    public String[] getTags_name() {
        return tags_name;
    }

    public void setTags_name(String[] tags_name) {
        this.tags_name = tags_name;
    }

    public byte[] getBody() {
        return body;
    }

    public void setBody(byte[] body) {
        this.body = body;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
