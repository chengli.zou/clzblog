package site.clzblog.user.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import site.clzblog.entity.User;

/**
 * @Desc User repository
 * @Author chengli.zou
 * @CreateDate 6/14/18
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
}
