package site.clzblog.user.repo;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import site.clzblog.entity.BlogComments;

import java.util.List;

@CacheConfig(cacheNames = "postCache")
@Repository
public interface CommentsRepo extends JpaRepository<BlogComments, Long> {
    @Cacheable
    List<BlogComments> findByArticleId(long articleId);

    @Cacheable
    List<BlogComments> findByArticleIdAndFloorIsGreaterThan(long articleId, Integer floor);

    void deleteAllByArticleId(long articleId);
}
