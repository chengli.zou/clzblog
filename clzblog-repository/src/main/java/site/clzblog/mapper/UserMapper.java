package site.clzblog.mapper;

import site.clzblog.entity.User;

/**
 * @Desc User dao implement
 * @Author chengli.zou
 * @CreateDate 6/15/18
 */
public interface UserMapper {
    User selectById(Integer id);
}
