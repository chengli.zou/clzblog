package site.clzblog.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import site.clzblog.entity.BlogArticles;
import site.clzblog.vo.ArticlesData;
import site.clzblog.vo.PostsData;

import java.util.List;

@Repository
@CacheConfig(cacheNames = "postCache")
public interface CommonMapper {
    @Cacheable
    String selectGroupConcatTags();

    @Cacheable
    String selectGroupConcatFromTags();

    @Cacheable
    Long selectArticlesCountByState(Integer state);

    List<BlogArticles> selectArticlesByState(@Param("state") Integer state, @Param("offset") Integer offset, @Param("rows") Integer rows);

    @Cacheable
    List<PostsData> selectPostsByState(@Param("state") Integer state, @Param("offset") Integer offset, @Param("rows") Integer rows);

    @Cacheable
    List<Integer> postYears();

    @Cacheable
    List<PostsData> selectArchiveByStateAndYear(@Param("state") Integer state, @Param("year") Integer year);

    @Cacheable
    List<ArticlesData> selectArticlesNoBody(@Param("offset") Integer offset, @Param("rows") Integer rows);

    @Cacheable
    Integer selectMaxFloorByArticleId(Long articleId);
}
