package site.clzblog.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import site.clzblog.entity.SysUser;

/**
 * @Desc
 * @Author chengli.zou
 * @CreateDate 6/15/18
 */
@Repository
@CacheConfig(cacheNames = "postCache")
public interface SysUserMapper {
    @Cacheable
    SysUser findByName(String name);

    void updateStatusByName(@Param("status") String status, @Param("name") String name);

    String findStatusByUsername(String username);

    void updateStatus(String status);

    @Cacheable
    Integer getCurrentUserId(String username);

    @Cacheable
    SysUser getCurrentUser(String name);
}
