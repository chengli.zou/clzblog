package site.clzblog.admin.repo;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import site.clzblog.entity.BlogArticles;

import java.util.Optional;

@CacheConfig(cacheNames = "postCache")
@Repository
public interface BlogArticlesRepository extends PagingAndSortingRepository<BlogArticles, Integer> {

    @Cacheable
    @Override
    Optional<BlogArticles> findById(Integer integer);
}
