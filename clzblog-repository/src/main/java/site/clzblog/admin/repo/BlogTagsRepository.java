package site.clzblog.admin.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import site.clzblog.entity.BlogTags;

@Repository
public interface BlogTagsRepository extends JpaRepository<BlogTags, Integer> {
    BlogTags findByName(String name);
}
