$(function () {
    $.get("/articles/count", function (count) {
        sessionStorage.setItem("article_sum", count);
    });

    $.ajax({
        url: "/articles/top",
        type: "get",
        data: {
            pageNo: 1,
            pageSize: 5
        },
        success: function (data) {
            var elements = "";
            $.each(data, function (index, object) {
                var title = object.title;
                if (title.length > 24) {
                    title = title.substr(0, 24) + "...";
                }
                var element = "<tr>" +
                    "<td title='" + object.title + "'>" + title + "</td>" +
                    "<td>" + object.replies + "</td>" +
                    "<td>" + object.date + "</td>" +
                    "<td>" +
                    "&nbsp;<a class='btn btn-xs btn-primary' href='../../article.html?id=" + object.id + "&token=administrator' role='button'>view</a>" +
                    "&nbsp;<a class='btn btn-xs btn-warning' href='/admin/new-article?id=" + object.id + "' role='button'>edit</a>" +
                    "&nbsp;<a onclick='delete_article(this, " + object.id + ")' class='btn btn-xs btn-danger' href='javascript:void(0);' role='button'>delete</a>" +
                    "</td>" +
                    "</tr>";
                elements += element;
            });
            $("#index-articles-body").html(elements);
        }
    });
});

function delete_article(self, id) {
    $("#confirm-modal-title").text("Delete Article Confirm");
    $("#confirm-modal-body").html("<h3>Confirm delete this article?</h3>");
    $("#confirm-modal").modal("show");
    $("#confirm-modal-btn").unbind().click(function () {
        $.ajax({
            url: "/articles/delete",
            type: "post",
            data: {
                id: id
            },
            success: function () {
                $(self).parent().parent().remove();
                var sum = Number(sessionStorage.getItem("article_sum")) - 1;
                sessionStorage.setItem("article_sum", sum.toString());
                $("#article-sum").text(sum);
            },
            error: function (error) {
                console.log(error);
            }
        });
        $("#confirm-modal").modal("hide");
    });
}