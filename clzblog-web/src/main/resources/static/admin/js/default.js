$(function () {
    $("#articles-create-new").click(function () {
        window.location.href = "/admin/new-article";
    });

    $("#commenters-new-article").click(function () {
        window.location.href = "/admin/new-article";
    });

    $("#approved-new-article").click(function () {
        window.location.href = "/admin/new-article";
    });



    var sum = sessionStorage.getItem("article_sum");
    if (sessionStorage.getItem("article_sum") != null) {
        $("#article-sum").text(sum);
    } else {
        $.get("/articles/count", function (count) {
            sessionStorage.setItem("article_sum", count);
        });
    }
});

function getUrlParamByName(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}