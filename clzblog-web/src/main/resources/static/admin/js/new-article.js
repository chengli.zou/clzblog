$(document).ready(function () {
    window.onbeforeunload = function (e) {
        var e = window.event || e;
        e.returnValue = ("Are you sure leave current page？");
        sessionStorage.removeItem("save_article_id");
    }

    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-result': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $('.summernote').summernote({
        height: 384
    })

    load_tags();// Initial tags

    var url_param_id = getUrlParamByName("id");
    if (null != url_param_id) {
        $.ajax({
            url: "/articles/edit",
            type: "get",
            data: {
                id: url_param_id
            },
            beforeSend: function () {
                $("#progress-modal-title").text("Loading");
                $("#progress-modal").modal("show");
                document.getElementById("progress-bar").style.width = 0 + "%";
            },
            success: function (data) {
                document.getElementById("progress-bar").style.width = 50 + "%";
                sessionStorage.setItem("save_article_id", url_param_id);
                $("#title").val(data.title);
                $("#intro").val(data.intro);
                chosen_multiple_select_initial("#tags", data.tags_ids);
                $('.summernote').code(decodeURIComponent($.base64.decode(data.body)));
                $("#is-publish").prop("checked", data.state == 1 ? true : false);
                document.getElementById("progress-bar").style.width = 100 + "%";
                setTimeout(function () {
                    $("#progress-modal").modal("hide");
                }, 256)
            }
        });
    }

    /**
     * Submit button click event
     */
    $("#btn-submit").unbind().click(function () {

        var article_title = $("#title").val();
        var article_intro = $("#intro").val();
        var tags = document.getElementById("tags");
        var tags_ids = new Array();
        var tags_name = new Array();
        for (var i = 0; i < tags.length; i++) {
            if (tags.options[i].selected) {
                tags_ids.push(tags.options[i].value);
                tags_name.push(tags.options[i].text);
            }
        }
        var article_html_code = $('.summernote').code();

        if (article_title != "" && article_intro != "" && tags_ids.length != 0 && article_html_code != "") {
            var is_publish = $("#is-publish").is(":checked");
            var data = {
                id: sessionStorage.getItem("save_article_id"),
                title: article_title,
                intro: article_intro,
                tags_ids: tags_ids,
                tags_name: tags_name,
                body: $.base64.encode(encodeURIComponent(article_html_code)),
                state: is_publish ? 1 : 0
            };

            $("#confirm-modal-title").text("Save/Publish Confirm");
            $("#confirm-modal-body").html("<h3>You confirm article edit finish?</h3>");
            $("#confirm-modal").modal("show");
            $("#confirm-modal-btn").unbind().click(function () {
                $.ajax({
                    url: "/articles/create",
                    type: "post",
                    contentType: "application/json",
                    dataType: "text",
                    data: JSON.stringify(data),
                    beforeSend: function () {
                        $("#progress-modal-title").text((is_publish ? "Publish" : "Save") + " Processing");
                        $("#progress-modal").modal("show");
                        document.getElementById("progress-bar").style.width = 0 + "%";
                    },
                    success: function (id) {
                        if (sessionStorage.getItem("save_article_id") == null) {
                            var sum = Number(sessionStorage.getItem("article_sum")) + 1;
                            sessionStorage.setItem("article_sum", sum.toString());
                            $("#article-sum").text(sum);
                        }

                        sessionStorage.setItem("save_article_id", id);

                        document.getElementById("progress-bar").style.width = 100 + "%";
                        $("#progress-modal-title").text((is_publish ? "Publish" : "Save") + " Successfully");
                        if (is_publish) {
                            sessionStorage.removeItem("save_article_id");
                            $("#title").val("");
                            $("#intro").val("");
                            tags_ids = new Array();
                            load_tags()
                            $('.summernote').code("");
                            $("#is-publish").prop("checked", false);
                        }
                    },
                    error: function () {
                        document.getElementById("progress-bar").style.width = "0%";
                        $("#progress-modal-title").text((is_publish ? "Publish" : "Save") + " Failure");
                    }
                });
            });
        } else {
            $("#tip-modal-title").text("Publish Tip");
            $("#tip-modal-body").html("<h3>Cannot be empty, You please check input text.</h3>");
            $("#tip-modal").modal("show");
        }


    });


    $('[data-toggle="offcanvas"]').click(function () {
        $('#side-menu').toggleClass('hidden-xs');
    });


});

function load_tags() {
    $.ajax({
        url: "/tags/load",
        type: "get",
        async: false,
        dataType: "json",
        success: function (tags) {
            $("#tags").empty();
            $.each(tags, function (index, tag) {
                $("#tags").append('<option value="' + tag.id + '">' + tag.name + '</option>');
            });
            $("#tags").trigger("chosen:updated");
        }
    });
}

/**
 * Chosen plugin multiple select initial function
 * @param select
 * @param arr
 */
function chosen_multiple_select_initial(select, arr) {
    var len = arr.length;
    for (var i = 0; i < len; i++) {
        $(select + ' option[value=' + arr[i] + ']').attr('selected', 'selected');
    }
    $(select).trigger('chosen:updated');
}