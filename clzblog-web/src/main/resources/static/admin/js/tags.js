$(document).ready(function () {
    $.ajax({
        url: "/tags/initial",
        type: "get",
        async: false,
        dataType: "json",
        beforeSend: function () {
            $("#progress-modal-title").text("Tags Loading");
            $("#progress-modal").modal("show");
            document.getElementById("progress-bar").style.width = 0 + "%";
        },
        success: function (tags) {
            var elements = "";
            $.each(tags, function (index, tag) {
                var delete_element = "";
                if (tag.status == "in use") {
                    delete_element = " <button type='button' class='btn btn-xs btn-danger disabled' onclick='tag_delete_click(this," + (tag.id) + ")'>delete</button>";
                } else {
                    delete_element = " <button type='button' class='btn btn-xs btn-danger' onclick='tag_delete_click(this," + (tag.id) + ")'>delete</button>";
                }
                var element = "<tr>" +
                    "<td>" + tag.name + "</td>" +
                    "<td><span class='label label-success'>" + tag.status + "</span></td>" +
                    "<td>" + tag.created + "</td>" +
                    "<td>" +
                    "<button type='button' class='btn btn-xs btn-warning' onclick='tag_edit_click(this," + (tag.id) + ")'>edit</button>" +
                    delete_element +
                    "</td>" +
                    "</tr>";
                elements += element;
                if (index < 100) {
                    document.getElementById("progress-bar").style.width = index + "%";
                }
            });
            $("#tags-table-body").html(elements);
            $("#textarea-tags").val('');
            $("#progress-modal-title").text("Tags Loaded");
            document.getElementById("progress-bar").style.width = 100 + "%";
            setInterval(function () {
                $("#progress-modal").modal("hide");
            }, 500);
        }
    });

    $("#btn-save-tags").click(function () {
        var tags = $("#textarea-tags").val();
        if (tags === "") {
            $("#tip-modal-title").text('Save tags tip');
            $("#tip-modal-body").html("<h3 style='text-align: center'>Tags cannot be empty. Your check textarea!</h3>");
            $("#tip-modal").modal('show');
            return;
        }

        $("#progress-modal-title").text("Tags Saving");
        $("#progress-modal").modal("show");

        var process_bar_width = 0;
        document.getElementById("progress-bar").style.width = process_bar_width + "%";

        var tagArr = tags.split(',');
        var tagsBodyElement = $("#tags-table-body").html();
        var elements = "";
        var warning_elements = "";
        var tag_arr_len = tagArr.length;
        var success_num = 0;
        var failure_num = 0;
        var error_num = 0;
        for (var i = 0; i < tag_arr_len; i++) {

            var name = tagArr[i];
            $.ajax({
                url: "/tags/exists",
                type: "get",
                async: false,
                data: {
                    name: name
                },
                success: function (flag) {
                    if (!flag) {
                        $.ajax({
                            url: "/tags/save",
                            type: "post",
                            async: false,
                            dataType: "json",
                            data: {
                                name: name,
                                status: "not use"
                            },
                            success: function (tag) {
                                var delete_element = "";
                                if (tag.status == "in use") {
                                    delete_element = " <button type='button' class='btn btn-xs btn-danger disabled' onclick='tag_delete_click(this," + (tag.id) + ")'>delete</button>";
                                } else {
                                    delete_element = " <button type='button' class='btn btn-xs btn-danger' onclick='tag_delete_click(this," + (tag.id) + ")'>delete</button>";
                                }
                                var element = "<tr>" +
                                    "<td>" + tag.name + "</td>" +
                                    "<td><span class='label label-success'>" + tag.status + "</span></td>" +
                                    "<td>Just now</td>" +
                                    "<td>" +
                                    "<button type='button' class='btn btn-xs btn-warning' onclick='tag_edit_click(this," + (tag.id) + ")'>edit</button>" +
                                    delete_element +
                                    "</td>" +
                                    "</tr>";

                                var success_element = "<div class='alert alert-success'>" +
                                    "<a href='#' class='close' data-dismiss='alert'>&times;</a>" +
                                    "<strong>Success！</strong>Tag \'" + name + "\' Saved!</div>";
                                success_num++;
                                elements += element;

                                warning_elements += success_element;
                                process_bar_width += (100 / tag_arr_len);
                                document.getElementById("progress-bar").style.width = process_bar_width + "%";
                                console.log(process_bar_width);
                            },
                            error: function (error) {
                                error_num++;
                                process_bar_width -= (100 / tag_arr_len);
                                document.getElementById("progress-bar").style.width = process_bar_width + "%";
                                console.log(error);
                            }
                        });
                    } else {
                        failure_num++;
                        var warning_element = "<div class='alert alert-warning'>" +
                            "<a href='#' class='close' data-dismiss='alert'>&times;</a>" +
                            "<strong>Warning！</strong>Tag \'" + name + "\' exists! Save failure! You reinput name try again.</div>";
                        warning_elements += warning_element;
                        process_bar_width -= (100 / tag_arr_len);
                        document.getElementById("progress-bar").style.width = process_bar_width + "%";
                    }
                }
            });
        }
        $("#save_tags_area_warning").html(warning_elements);
        $("#tags-table-body").html(tagsBodyElement + elements);
        $("#textarea-tags").val('');
        $("#progress-modal-title").text("Tags saved");
        document.getElementById("progress-bar").style.width = process_bar_width + "%";
    });

});

function tag_edit_click(self, id) {
    var current_name = $(self).parent().parent().children()[0].innerText;
    $("#edit-modal-title").text('Edit tags modal');
    $("#edit-modal-body").html("<input id='edit_input' name='edit_input' class='form-control' type='text' placeholder='" + current_name + "'/>");
    $("#edit-modal").modal('show');
    $("#edit-confirm").unbind().click(function () {
        var new_curr_name = $("#edit_input").val();

        if (new_curr_name === "") {
            $("#edit-modal").modal('hide');
        } else {
            $.ajax({
                url: "/tags/exists",
                type: "get",
                async: false,
                data: {
                    name: new_curr_name
                },
                success: function (flag) {
                    if (!flag) {
                        $.ajax({
                            url: "/tags/update",
                            type: "put",
                            dataType: "text",
                            data: {
                                id: id,
                                name: new_curr_name
                            },
                            success: function () {
                                $("#edit-modal").modal('hide');
                                $(self).parent().parent().children()[0].innerText = new_curr_name;
                            },
                            error: function () {
                                $("#edit-modal").modal('hide');
                                $("#tip-modal-title").text('Edit tags tip');
                                $("#tip-modal-body").html("<h3>Failure!</h3>");
                                $("#tip-modal").modal('show');
                            }
                        });
                    } else {
                        var warning = "<div class='alert alert-warning'>" +
                            "<a href='#' class='close' data-dismiss='alert'>&times;</a>" +
                            "<strong>Warning！</strong>Tag name exists!You reset input name.</div>";
                        $("#edit-modal-body").html($("#edit-modal-body").html() + warning);
                    }
                }
            });

        }
    });
}

function tag_delete_click(self, id) {
    $("#confirm-modal-title").text("Delete Tag Confirm");
    $("#confirm-modal-body").html("<h3>Confirm delete this tag?</h3>");
    $("#confirm-modal").modal("show");
    $("#confirm-modal-btn").unbind().click(function () {
        $.ajax({
            url: "/tags/delete",
            type: "post",
            dataType: "text",
            data: {id: id},
            success: function () {
                $(self).parent().parent().remove();
            },
            error: function () {
                $("#tip-modal-title").text('Delete tags tip');
                $("#tip-modal-body").html("<h3>Failure!</h3>");
                $("#tip-modal").modal('show');
            }
        });
    });

}