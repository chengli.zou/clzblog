$(function () {
    $.get("/articles/count", function (count) {
        var pageSize = 5;
        $('#pagination').twbsPagination({
            totalPages: Math.ceil(count / pageSize),
            startPage: 1,
            visiblePages: 5,
            first: '«',
            prev: '←',
            next: '→',
            last: '»',
            onPageClick: function (event, page) {
                $.ajax({
                    url: "/articles/page",
                    type: "get",
                    async: true,
                    data: {
                        pageNo: page,
                        pageSize: pageSize
                    },
                    success: function (articles) {
                        if (null != articles) {
                            var elements = "";
                            $.each(articles, function (index, object) {
                                var state = object.state == 1 ? "<span class='label label-success label-sm'>Active</span>" : "<span class='label label-warning label-sm'>Inactive</span>";
                                var element = "<div class='row article-row'>" +
                                    "    <div class='col-xs-2 col-sm-1 col-md-1 status-padding'>" +
                                    state +
                                    "    </div>" +
                                    "    <div class='col-xs-10 col-sm-6 col-md-8 article-title'>" +
                                    "    <p>" + object.title + "</p>" +
                                    "<small>Created " + (new Date(object.createTime).toDateString()) + "</small>" +
                                    "</div>" +
                                    "<div class='col-xs-10 col-sm-5 col-md-3 col-xs-offset-2 col-sm-offset-0 col-md-offset-0 col-lg-offset-0'>" +
                                    "    <div class='article-actions'>" +
                                    "    <a class='btn btn-xs btn-default' href='../../article.html?id=" + object.id + "&token=administrator' role='button'>" +
                                    "    <span class='glyphicon glyphicon-folder-open' aria-hidden='true'>&nbsp;View</span>" +
                                    "</a> " +
                                    "" +
                                    "<a class='btn btn-xs btn-default' href='/admin/new-article?id=" + object.id + "' role='button'>" +
                                    "    <span class='glyphicon glyphicon-pencil' aria-hidden='true'>&nbsp;Edit</span>" +
                                    "</a> " +
                                    "" +
                                    "<a  onclick='delete_article(this," + object.id + ")' class='btn btn-xs btn-default' href='javascript:void(0);' role='button'>" +
                                    "    <span class='glyphicon glyphicon-remove' aria-hidden='true'>&nbsp;Delete</span>" +
                                    "</a> " +
                                    "</div>" +
                                    "</div>" +
                                    "</div>" +
                                    "<hr>";
                                elements += element;
                            });
                            $("#article-rows").html(elements);
                        }
                    }
                });
            }
        });
    });
})

function delete_article(self, id) {
    $("#confirm-modal-title").text("Delete Article Confirm");
    $("#confirm-modal-body").html("<h3>Confirm delete this article?</h3>");
    $("#confirm-modal").modal("show");
    $("#confirm-modal-btn").unbind().click(function () {
        $.ajax({
            url: "/articles/delete",
            type: "post",
            data: {
                id: id
            },
            beforeSend: function() {
                $("#progress-modal-title").text("Deleting");
                $("#progress-modal").modal("show");
                document.getElementById("progress-bar").style.width = 0 + "%";
            },
            success: function () {
                $(self).parent().parent().parent().next().remove();
                $(self).parent().parent().parent().remove();
                var sum = Number(sessionStorage.getItem("article_sum")) - 1;
                sessionStorage.setItem("article_sum", sum.toString());
                $("#article-sum").text(sum);
                $("#progress-modal-title").text("Deleted");
                document.getElementById("progress-bar").style.width = 100 + "%";
            },
            error: function (error) {
                console.log(error);
            }
        });
        $("#confirm-modal").modal("hide");
    });
}