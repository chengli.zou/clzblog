var form = $('#comments_form');
$(function () {
    form.bootstrapValidator({
        message: 'Input value is illegal',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            comment_msg: {
                validators: {
                    notEmpty: {
                        message: 'Comment message cannot be empty'
                    },
                    stringLength: {
                        min: 3,
                        max: 1000,
                        message: 'Please input 3 ~ 1000 char'
                    }
                }
            }
        }
    });

    $("#form_login").bootstrapValidator({
        message: 'Input value is illegal',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
                validators: {
                    notEmpty: {
                        message: 'Username cannot be empty'
                    },
                    emailAddress: {
                        message: 'Must contains @ example:chengli.zou@gmail.com'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'Password cannot be empty'
                    },
                    stringLength: {
                        min: 6,
                        max: 16,
                        message: 'Please input 6 ~ 16 char'
                    }
                }
            }
        }
    });

    $('#myModal').each(function (i) {
        var $clone = $(this).clone().css('display', 'block').appendTo('body');
        var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
        top = top > 0 ? top : 0;
        $clone.remove();
        $(this).find('.modal-content').css("margin-top", top);
    });

    function getUrlParamByName(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return unescape(r[2]);
        return null;
    }

    var id = getUrlParamByName("id");
    if (null != id && "" != id) {
        $.ajax({
            url: "/articles/view",
            type: "get",
            data: {
                id: id
            },
            beforeSend: function () {
                $("#myModal").modal("show");
            },
            success: function (data) {
                if (data.state == 1 || getUrlParamByName("token") == "administrator") {
                    $("#view-article-title").html(data.title);
                    $("#view-article-body").html(decodeURIComponent($.base64.decode(data.body)));
                }

                if (authenticate()) {
                    loadComments(id, null);
                } else {
                    loadComments(id, sessionStorage.getItem("curr_user_id"));
                    $("#login_info_panel").html("<li><a href='javascript:void(0);'>Welcome " + sessionStorage.getItem("curr_full_name") + "</a></li>");
                }

                $("#myModal").modal("hide");
            }
        });
    } else {
        window.location.href = "404";
    }


    $("#submit_comment_btn").unbind().click(function () {
        var bv = form.data('bootstrapValidator');
        bv.validate();
        if (bv.isValid()) {
            if (authenticate()) {
                login(id);
            } else {
                comment(id);
            }
        }
    });
});


function authenticate() {
    var flag = false;
    $.ajax({
        url: "/comments/authenticate",
        type: "get",
        async: false,
        success: function (isAnonymousUser) {
            flag = isAnonymousUser;
        },
        error: function (error) {
            console.log(error);
        }
    });
    return flag;
}

function login(id) {
    $("#login-reg-modal").modal('show');
    $("#sign_btn").unbind().click(function () {
        var bv = $("#form_login").data("bootstrapValidator");
        bv.validate();
        if (bv.isValid()) {
            $.ajax({
                url: "/login",
                type: "post",
                async: false,
                dataType: "text",
                data: "username=" + $("#username").val() + "&password=" + $("#password").val(),
                success: function (status) {
                    if ("admin_login_success" === status) {
                        window.location.href = "/admin/index";
                    } else if ("user_login_success" === status) {
                        setLoginInfo();

                        comment(id);

                        $("#login-reg-modal").modal('hide');
                    } else {
                        alert('Login failure, Please you check username and password is correct');
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    });
}

function setLoginInfo() {
    $.get("/comments/commenter", function (commenter) {
        var id = commenter.id;
        var name = commenter.nickname;
        if (name != undefined) {
            sessionStorage.setItem("curr_user_id", id);
            sessionStorage.setItem("curr_full_name", name);
            $("#login_info_panel").html("<li><a href='javascript:void(0);'>Welcome " + name + "</a></li>");
        } else {
            sessionStorage.setItem("curr_user_id", id);
            sessionStorage.setItem("curr_full_name", name);
        }
    });
}

function loadComments(id, uid) {
    $.ajax({
        url: "/comments/load",
        type: "get",
        data: {
            id: id
        },
        success: function (data) {
            var elements = "";
            $.each(data, function (index, object) {
                var floor = object.floor;
                var fromUid = object.fromUid;
                var replyElement = "<a id='reply' href='javascript:void(0);' onclick='reply(" + id + "," + uid + "," + fromUid + ",\"" + object.fullName + "\")'><div class='col-md-1'>Reply</div></a>";
                if (uid != null) {
                    if (fromUid == uid) {
                        replyElement = "<a id='reply' href='javascript:void(0);' onclick='del(" + object.id + "," + uid + ", " + id + "," + floor + ")'><div class='col-md-1'>Delete</div></a>";
                    }
                } else {
                    replyElement = "<a title='Reply or delete,You need click me login.' href='javascript:void(0);' onclick='login(" + id + ")'><div class='col-md-1'>Login</div></a>";
                }
                var datetime = object.createTime;
                var date_str = datetime.split('T')[0].replace(new RegExp(/-/gm), "/");
                var toName = object.toName;
                var fullName = object.fullName;
                var commenter = toName == "" ? fullName : (fullName + " @ " + toName);
                var element = "<div id='comment_rows' class='row'>" +
                    "<div class='col-md-11'>" +
                    "<p>" + object.content + "</p>" +
                    "<footer style='border-bottom: 1px dotted black'>" +
                    "<small><b>" + commenter + "</b> Commented on: " + new Date(date_str).toDateString() + "</small>" +
                    "</footer>" +
                    "<hr>" +
                    "</div>" + replyElement +
                    "<div class='col-md-1 comment-num'> " + floor + "F</div>" +
                    "</div>";
                elements += element;
            });
            $("#comments_body").html(elements != "" ? elements : "<div class='row'><div class='col-md-12'><p>Want to say something to the blogger?</p></div></div>");
        }
    });
}

function del(id, uid, aid, floor) {
    $("#confirm-modal").modal("show");
    $("#confirm-modal-title").text("Confirm Delete Tip");
    $("#confirm-modal-body").html("<h3>You confirm delete this comment?</h3>");
    $("#confirm-modal-btn").unbind().click(function () {
        $.ajax({
            url: "/comments/delete",
            type: "post",
            async: false,
            data: {
                id: id,
                articleId: aid,
                floor: floor
            },
            success: function () {
                loadComments(aid, uid);
            }
        });
    });
}

function reply(id, uid, toUid, nickname) {
    $("#confirm-modal").modal("show");
    $("#confirm-modal-title").text("Reply " + nickname + ":");
    $("#confirm-modal-body").html("<textarea class='form-control' placeholder='Reply content' id='reply_msg'></textarea>");
    $("#confirm-modal-btn").unbind().click(function () {
        var commentData = {
            articleId: id,
            type: "",
            fullName: sessionStorage.getItem("curr_full_name"),
            content: $("#reply_msg").val(),
            fromUid: uid,
            toUid: toUid,
            toName: nickname
        };
        addComment(commentData);
    });
}

function comment(id) {
    var uid = sessionStorage.getItem("curr_user_id");
    var fullName = sessionStorage.getItem("curr_full_name");
    var content = $("#comment_msg").val();
    if (content == "") {
        loadComments(id, uid);
    } else {
        var commentData = {
            articleId: id,
            type: "",
            fullName: fullName,
            content: content,
            fromUid: uid,
            toUid: null,
            toName: ""
        };
        addComment(commentData);
    }
}

function addComment(data) {
    $.ajax({
        url: "/comments/add",
        type: "post",
        contentType: "application/json",
        data: JSON.stringify(data),
        success: function (flag) {
            if (flag) {
                $("#comment_msg").val('');
                loadComments(data.articleId, data.fromUid);
            } else {
                alert('Comment failure');
            }
        }
    });
}