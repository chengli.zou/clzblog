$(function () {
    $("#form_login").bootstrapValidator({
        message: 'Input value is illegal',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
                validators: {
                    notEmpty: {
                        message: 'Username cannot be empty'
                    },
                    emailAddress: {
                        message: 'Must contains @ example:chengli.zou@gmail.com'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'Password cannot be empty'
                    },
                    stringLength: {
                        min: 6,
                        max: 16,
                        message: 'Please input 6 ~ 16 char'
                    }
                }
            }
        }
    });

    $("#sign_btn").unbind().click(function () {
        var bv = $("#form_login").data("bootstrapValidator");
        bv.validate();
        var username = $("#username").val();
        if (bv.isValid()) {
            $.ajax({
                url: "/online",
                type: "get",
                async:false,
                data: {
                    username: username
                },
                success: function (flag) {
                    if(flag) {
                        alert('Your is online,Please !');
                    } else {
                        $.ajax({
                            url: "/login",
                            type: "post",
                            dataType: "text",
                            data: "username=" + username + "&password=" + $("#password").val(),
                            success: function (status) {
                                console.log(status);
                                if ("admin_login_success" === status) {
                                    window.location.href = "/admin/index";
                                } else if ("user_login_success" === status) {
                                    window.location.href = "/";
                                } else {
                                    alert('Login failure');
                                    window.location.reload();
                                }
                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });
                    }
                }
            });

        }
    });
});

