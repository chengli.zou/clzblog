$(function () {
    var curr_full_name = sessionStorage.getItem("curr_full_name");
    if (curr_full_name != "undefined") {
        $("#login_info_panel").html("<li><a href='javascript:void(0);'>Welcome " + curr_full_name + "</a></li>");
    }

    $('#myModal').each(function (i) {
        var $clone = $(this).clone().css('display', 'block').appendTo('body');
        var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
        top = top > 0 ? top : 0;
        $clone.remove();
        $(this).find('.modal-content').css("margin-top", top);
    });

    $("#myModal").modal("show");

    $.get("/archive/years", function (years) {
        var elements = "<h1>Archive</h1>";
        for (var i = 0; i < years.length; i++) {
            var post_elements = "";
            var year = years[i];
            $.ajax({
                url: "/archive/year",
                type: "get",
                async: false,
                data: {
                    year: year
                },
                success: function (posts) {
                    $.each(posts, function (index, post) {
                        var datetime = post.createTime;
                        var date_str = datetime.split('T')[0].replace(new RegExp(/-/gm) ,"/");
                        var post_element = "<div class='row'>" +
                            "<div class='col-md-9'><a href='article.html?id=" + post.id + "'>" + post.title + "</a></div>" +
                            "<div class='col-md-3 date'>" + new Date(date_str).toDateString() + "</div>" +
                            "</div>";
                        post_elements += post_element;
                    })
                }
            });
            var element = "<article>" +
                "<h3>" + year + "</h3>" +
                post_elements +
                "<hr>" +
                "</article>";
            elements += element;
        }
        $("#archive_body").html(elements);
        $("#myModal").modal("hide");
    })
});