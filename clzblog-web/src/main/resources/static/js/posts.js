$(function () {
    $.get("/comments/commenter", function (commenter) {
        var id = commenter.id;
        var name = commenter.nickname;
        if (name != undefined) {
            sessionStorage.setItem("curr_user_id", id);
            sessionStorage.setItem("curr_full_name", name);
            $("#login_info_panel").html("<li><a href='javascript:void(0);'>Welcome " + name + "</a></li>");
        } else {
            sessionStorage.setItem("curr_user_id", id);
            sessionStorage.setItem("curr_full_name", name);
        }
    });

    $.get("/posts/count", function (count) {
        var pageSize = 2;
        var totalPages = Math.ceil(count / pageSize);
        $('#pagination').twbsPagination({
            totalPages: totalPages,
            startPage: 1,
            visiblePages: 4,
            first: '«',
            prev: '←',
            next: '→',
            last: '»',
            onPageClick: function (event, page) {
                $.ajax({
                    url: "/posts/page",
                    type: "get",
                    data: {
                        pageNo: page,
                        pageSize: pageSize
                    },
                    success: function (posts) {
                        var postElements = "";
                        $.each(posts, function (index, post) {
                            var datetime = post.createTime;
                            var date_str = datetime.split('T')[0].replace(new RegExp(/-/gm), "/");
                            var tagsNameElements = "";
                            var tagsName = post.tagsName.split(',');
                            for (var i = 0; i < tagsName.length; i++) {
                                var tagsNameElement = "";
                                tagsNameElement = "<span class='label label-default'>" + tagsName[i] + "</span> ";
                                tagsNameElements += tagsNameElement;
                            }
                            var postElement = "<article>" +
                                "<header><h2>" + post.title + "</h2></header>" +
                                "<footer>" +
                                "<small>Posted on " + new Date(date_str).toDateString() + "</small>" +
                                "</footer>" +
                                "<div class='lead'>" + post.intro +
                                "<a href='article.html?id=" + post.id + "'>Read More</a>" +
                                "</div>" +
                                "<footer>" +
                                tagsNameElements +
                                "</footer>" +
                                "<hr>" +
                                "</article>";
                            postElements += postElement;
                        });
                        $("#posts_body").html(postElements);
                    }
                });
            }
        });
    });


});