package site.clzblog.destroy;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import site.clzblog.global.constant.GlobalConstant;
import site.clzblog.user.service.UserService;

import javax.annotation.PreDestroy;

/**
 * @Desc Spring boot exit
 * @Author chengli.zou
 * @CreateDate 7/4/18
 */
@Component
public class SpringBootExit {
    private Log log = LogFactory.getLog(getClass());

    @Autowired
    private UserService userService;

    @PreDestroy
    public void destroy() {

        userService.updateStatus(GlobalConstant.OFFLINE.getName());
        log.info("=====================================SpringBootExit=====================================");

    }
}
