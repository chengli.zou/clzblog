package site.clzblog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import site.clzblog.user.service.ArchiveService;
import site.clzblog.vo.PostsData;

import java.util.List;

/**
 * @Desc
 * @Author chengli.zou
 * @CreateDate 6/28/18
 */
@RestController
@RequestMapping("archive")
public class ArchiveController {
    @Autowired
    private ArchiveService archiveService;

    @GetMapping("years")
    public List<Integer> years() {
        return archiveService.years();
    }

    @GetMapping("year")
    public List<PostsData> year(@RequestParam("year") Integer year) {
        return archiveService.year(1, year);
    }
}
