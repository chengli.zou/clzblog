package site.clzblog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import site.clzblog.user.service.PostService;
import site.clzblog.vo.PostsData;

import java.util.List;

@RestController
@RequestMapping("posts")
public class PostsController {
    @Autowired
    private PostService postService;

    @GetMapping("count")
    public long count() {
        return postService.count(1);
    }

    @GetMapping("page")
    public List<PostsData> page(@RequestParam("pageNo")Integer pageNo, @RequestParam("pageSize")Integer pageSize) {
        return postService.page(1, pageNo, pageSize);
    }
}
