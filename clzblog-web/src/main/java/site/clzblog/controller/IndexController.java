package site.clzblog.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
    @RequestMapping("/")
    public String index() {
        return "posts";
    }

    @RequestMapping("ws")
    public String ws() {
        return "ws";
    }
}
