package site.clzblog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import site.clzblog.entity.BlogTags;
import site.clzblog.admin.service.BlogTagsService;
import site.clzblog.vo.TagData;

import java.util.List;

@RestController
@RequestMapping("tags")
public class TagsController {
    @Autowired
    private BlogTagsService blogTagsService;

    @PostMapping("save")
    public BlogTags save(@RequestParam("name") String name, @RequestParam("status") String status) {
        return blogTagsService.save(name, status);
    }

    @GetMapping("initial")
    public List<TagData> initial() {
        return blogTagsService.findAll();
    }

    @GetMapping("load")
    public List<BlogTags> load() {
        return blogTagsService.load();
    }

    @PostMapping("delete")
    public void delete(@RequestParam("id") Integer id) {
        try {
            blogTagsService.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @PutMapping("update")
    public void update(@RequestParam("id") Integer id, @RequestParam("name") String name) {
        try {
            blogTagsService.saveOrUpdate(id, name);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @GetMapping("exists")
    public boolean exists(@RequestParam("name") String name) {
        boolean flag = false;
        BlogTags byName = blogTagsService.findByName(name);
        if (byName != null) {
            flag = true;
        }
        return flag;
    }

    @GetMapping("find")
    public String find(@RequestParam("id") Integer id) {
        return blogTagsService.find(id);
    }
}
