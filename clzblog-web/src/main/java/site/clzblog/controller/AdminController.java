package site.clzblog.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Desc Administrator controller
 * @Author chengli.zou
 * @CreateDate 6/15/18
 */
@Controller
@RequestMapping("admin")
public class AdminController {
    @RequestMapping({"index", "/"})
    @PreAuthorize("hasRole('admin')")
    public String index() {
        return "admin/index";
    }

    @RequestMapping("new-article")
    @PreAuthorize("hasRole('admin')")
    public String newArticle() {
        return "admin/new-article";
    }

    @RequestMapping("articles")
    @PreAuthorize("hasRole('admin')")
    public String articles() {
        return "admin/articles";
    }

    @RequestMapping("approved")
    @PreAuthorize("hasRole('admin')")
    public String approved() {
        return "admin/approved";
    }

    @RequestMapping("commenters")
    @PreAuthorize("hasRole('admin')")
    public String commenters() {
        return "admin/commenters";
    }

    @RequestMapping("tags")
    @PreAuthorize("hasRole('admin')")
    public String tags() {
        return "admin/tags";
    }

    @RequestMapping("settings")
    @PreAuthorize("hasRole('admin')")
    public String settings() {
        return "admin/settings";
    }
}
