package site.clzblog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import site.clzblog.admin.service.BlogArticleService;
import site.clzblog.entity.BlogArticles;
import site.clzblog.vo.ArticleData;
import site.clzblog.vo.ArticlesData;
import site.clzblog.vo.IndexArticles;

import java.util.List;

@RestController()
@RequestMapping("articles")
public class ArticlesController {
    @Autowired
    private BlogArticleService blogArticleService;

    @PostMapping("create")
    public int create(@RequestBody ArticleData articleData) {
        return blogArticleService.save(articleData).getId();
    }

    @GetMapping("view")
    public BlogArticles view(@RequestParam("id") Integer id) {
        return blogArticleService.view(id);
    }

    @GetMapping("edit")
    public ArticleData edit(@RequestParam("id") Integer id) {
        return blogArticleService.edit(id);
    }

    @PostMapping("delete")
    public void delete(@RequestParam("id") Integer id) {
        blogArticleService.delete(id);
    }

    @GetMapping("count")
    public long count() {
        return blogArticleService.count();
    }

    @GetMapping("page")
    public List<ArticlesData> page(@RequestParam("pageNo") Integer pageNo, @RequestParam("pageSize") Integer pageSize) {
        return blogArticleService.page(pageNo, pageSize);
    }

    @GetMapping("top")
    public List<IndexArticles> top(@RequestParam("pageNo") Integer pageNo, @RequestParam("pageSize") Integer pageSize) {
        return blogArticleService.top(pageNo, pageSize);
    }
}
