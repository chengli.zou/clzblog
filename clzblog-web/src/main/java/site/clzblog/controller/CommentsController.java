package site.clzblog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import site.clzblog.entity.BlogComments;
import site.clzblog.entity.SysUser;
import site.clzblog.global.constant.GlobalConstant;
import site.clzblog.user.service.CommentService;
import site.clzblog.vo.CommentData;

import java.util.List;

@RestController
@RequestMapping("comments")
public class CommentsController {
    @Autowired
    private CommentService commentService;

    @GetMapping("authenticate")
    public boolean authenticate() {
        return SecurityContextHolder.getContext().getAuthentication().getName().equals(GlobalConstant.ANONYMOUS_USER.getName());
    }

    @GetMapping("commenter")
    public SysUser commenter() {
        return commentService.getCurrentUser(SecurityContextHolder.getContext().getAuthentication().getName());
    }

    @GetMapping("load")
    public List<BlogComments> load(@RequestParam("id") Long articleId) {
        return commentService.load(articleId);
    }

    @PostMapping("add")
    @PreAuthorize("hasRole('user')")
    public boolean add(@RequestBody CommentData commentData) {
        return commentService.add(commentData);
    }

    @PostMapping("delete")
    @PreAuthorize("hasRole('user')")
    public void delete(@RequestParam("id") Long id, @RequestParam("articleId") long articleId, @RequestParam("floor") Integer floor) {
        commentService.delete(id, articleId, floor);
    }
}
