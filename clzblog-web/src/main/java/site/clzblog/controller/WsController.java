package site.clzblog.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import site.clzblog.vo.ResponseMessage;

/**
 * @Desc
 * @Author chengli.zou
 * @Datetime 18-10-12 下午2:43
 **/
@Slf4j
@RestController
public class WsController {
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @PostMapping("/sendHello")
    public void sendHello(String name) {
        log.info("Transmission history -> {}", name);
        simpMessagingTemplate.convertAndSend("/topic/helloChannel", new ResponseMessage(name));
    }
}
