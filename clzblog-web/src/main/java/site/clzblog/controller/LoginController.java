package site.clzblog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import site.clzblog.user.service.UserService;


@Controller
public class LoginController {
    @Autowired
    private UserService userService;

    @RequestMapping("success")
    public ModelAndView success(SecurityContextHolderAwareRequestWrapper requestWrapper) {
        if (requestWrapper.isUserInRole("ROLE_user")) {
            return new ModelAndView(new RedirectView("/"));
        } else if (requestWrapper.isUserInRole("ROLE_admin")) {
            return new ModelAndView(new RedirectView("/admin/index"));
        }
        return new ModelAndView("login");
    }

    @RequestMapping("online")
    @ResponseBody
    public boolean online(@RequestParam("username") String username) {
        String status = userService.queryStatusByUsername(username);
        return status.equals("online");
    }
}