package site.clzblog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import site.clzblog.user.service.UserService;
import site.clzblog.vo.R;

/**
 * @Desc User controller
 * @Author chengli.zou
 * @CreateDate 6/14/18
 */
@Controller
@RequestMapping("/user/*")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("index")
    public String index() {
        return "index";
    }

    @GetMapping("hello")
    @ResponseBody
    public String helloWorld() {
        return "Hello World";
    }

    @GetMapping("list")
    @ResponseBody
    public R list() {
        try {
            return R.isOk().data(userService.list());
        } catch (Exception e) {
            return R.isFail(e);
        }
    }
}
