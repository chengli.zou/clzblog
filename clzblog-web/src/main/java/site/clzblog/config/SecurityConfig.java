package site.clzblog.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;

import site.clzblog.global.constant.GlobalConstant;
import site.clzblog.password.encoder.MyPasswordEncoder;
import site.clzblog.user.service.UserService;
import site.clzblog.user.service.impl.UserDetailsServiceImpl;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

/**
 * @Desc Security config
 * @Author chengli.zou
 * @CreateDate 6/14/18
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserService userService;

    @Bean
    UserDetailsService customUserService() {
        return new UserDetailsServiceImpl();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf() // Cross station
                .disable() // Close cross station check
                .authorizeRequests() // Verification strategy
                .antMatchers("/admin/**").hasRole("admin")
                .antMatchers("/user/**").hasRole("user")
                .and()
                .formLogin()
                .loginPage("/login.html")
                .successHandler((httpServletRequest, httpServletResponse, authentication) -> {
                    Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
                    for (GrantedAuthority authority : authorities) {
                        String s = authority.getAuthority();
                        if (GlobalConstant.ROLE_ADMIN.getName().equals(s)) {
                            loginResponse(httpServletResponse, "admin_login_success");
                        } else if (GlobalConstant.ROLE_USER.getName().equals(s)) {
                            loginResponse(httpServletResponse, "user_login_success");
                            userService.updateStatusByName(GlobalConstant.ONLINE.getName(), authentication.getName());
                        }
                    }

                }).failureHandler((httpServletRequest, httpServletResponse, e) -> loginResponse(httpServletResponse, e.getMessage()))
                .loginProcessingUrl("/login")
                .usernameParameter("username")
                .passwordParameter("password")
                .permitAll()
                .and()
                .logout()
                .logoutSuccessHandler((httpServletRequest, httpServletResponse, authentication) -> {
                    Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
                    for (GrantedAuthority authority : authorities) {
                        String s = authority.getAuthority();
                        if (GlobalConstant.ROLE_ADMIN.getName().equals(s)) {
                            //loginResponse(httpServletResponse, "admin_logout_success");
                        } else if (GlobalConstant.ROLE_USER.getName().equals(s)) {
                            //loginResponse(httpServletResponse, "user_logout_success");
                            userService.updateStatusByName(GlobalConstant.OFFLINE.getName(), authentication.getName());
                        }
                    }
                    httpServletResponse.sendRedirect("/login.html");
                })
                .logoutUrl("/logout").permitAll()
                .and()
                .exceptionHandling()
                .accessDeniedPage("/error");// No access
    }

    private void loginResponse(HttpServletResponse httpServletResponse, String s) throws IOException {
        httpServletResponse.setContentType("application/json;charset=utf-8");
        PrintWriter writer = httpServletResponse.getWriter();
        writer.print(s);
        writer.flush();
        writer.close();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customUserService()).passwordEncoder(new MyPasswordEncoder());
    }
}
