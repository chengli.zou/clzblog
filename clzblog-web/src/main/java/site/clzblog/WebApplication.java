package site.clzblog;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * @Desc Web application starter
 * @Author chengli.zou
 * @CreateDate 6/14/18
 */
@SpringBootApplication
@MapperScan("site.clzblog.mapper")
@EnableCaching
public class WebApplication {
    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);
    }
}
