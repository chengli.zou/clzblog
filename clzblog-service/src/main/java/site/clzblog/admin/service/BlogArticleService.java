package site.clzblog.admin.service;

import site.clzblog.entity.BlogArticles;
import site.clzblog.vo.ArticleData;
import site.clzblog.vo.ArticlesData;
import site.clzblog.vo.IndexArticles;

import java.util.List;

public interface BlogArticleService {
    BlogArticles save(ArticleData articleData);

    BlogArticles view(int id);

    long count();

    List<ArticlesData> page(Integer pageNo, Integer pageSize);

    ArticleData edit(Integer id);

    void delete(Integer id);

    List<IndexArticles> top(Integer pageNo, Integer pageSize);
}
