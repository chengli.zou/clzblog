package site.clzblog.admin.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import site.clzblog.admin.repo.BlogArticlesRepository;
import site.clzblog.admin.service.BlogArticleService;
import site.clzblog.admin.service.BlogTagsService;
import site.clzblog.entity.BlogArticles;
import site.clzblog.mapper.CommonMapper;
import site.clzblog.user.repo.CommentsRepo;
import site.clzblog.util.IntervalUtil;
import site.clzblog.vo.ArticleData;
import site.clzblog.vo.ArticlesData;
import site.clzblog.vo.IndexArticles;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class BlogArticleServiceImpl implements BlogArticleService {
    @Autowired
    private BlogArticlesRepository blogArticlesRepository;

    @Autowired
    private BlogTagsService blogTagsService;

    @Autowired
    private CommonMapper commonMapper;

    @Autowired
    private CommentsRepo commentsRepo;

    @Override
    public BlogArticles save(ArticleData articleData) {
        BlogArticles blogArticles = new BlogArticles();

        Integer id = articleData.getId();
        blogArticles.setTitle(articleData.getTitle());
        blogArticles.setIntro(articleData.getIntro());

        String tags = "";
        String tagsName = "";
        int[] tags_ids = articleData.getTags_ids();
        for (int i = 0; i < tags_ids.length; i++) {
            int tags_id = tags_ids[i];
            String tags_name = articleData.getTags_name()[i];
            blogTagsService.saveOrUpdate(tags_id, null, "in use");
            tags += tags_id + ",";
            tagsName += tags_name + ",";
        }
        tags = tags.substring(0, tags.lastIndexOf(","));
        tagsName = tagsName.substring(0, tagsName.lastIndexOf(","));

        if (null != id) {
            blogArticles.setId(id);
            updateNotUseTag(id, tags, true);
        } else {
            updateNotUseTag(null, tags, false);
        }

        blogArticles.setTags(tags);
        blogArticles.setTagsName(tagsName);
        blogArticles.setBody(articleData.getBody());
        blogArticles.setState(articleData.getState());

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        blogArticles.setCreateTime(timestamp);
        blogArticles.setUpdateTime(timestamp);

        blogArticles.setRemark("");
        return blogArticlesRepository.save(blogArticles);
    }

    private void updateNotUseTag(Integer id, String tags, boolean flag) {
        String groupConcatTags = commonMapper.selectGroupConcatTags() + (null != tags ? ("," + tags) : "");
        if (flag) {
            BlogArticles articles = blogArticlesRepository.findById(id).get();
            String[] oldTags = articles.getTags().split(",");
            for (int i = 0; i < oldTags.length; i++) {
                if (!tags.contains(oldTags[i])) {
                    groupConcatTags = groupConcatTags.replace(oldTags[i], "");
                }
            }
        }
        String groupConcatFromTags = commonMapper.selectGroupConcatFromTags();
        String[] produceTagIds = groupConcatFromTags.split(",");

        for (int i = 0; i < produceTagIds.length; i++) {
            String produceTagId = produceTagIds[i];
            if (!groupConcatTags.contains(produceTagId)) {
                blogTagsService.saveOrUpdate(Integer.valueOf(produceTagId), null, "not use");
            }
        }
    }

    @Override
    public BlogArticles view(int id) {
        return blogArticlesRepository.findById(id).get();
    }

    @Override
    public long count() {
        return blogArticlesRepository.count();
    }

    @Override
    public List<ArticlesData> page(Integer pageNo, Integer pageSize) {
        Integer offset = (pageNo - 1) * pageSize;
        Integer rows = pageSize;
        return commonMapper.selectArticlesNoBody(offset, rows);
    }

    @Override
    public ArticleData edit(Integer id) {
        BlogArticles article = blogArticlesRepository.findById(id).get();
        ArticleData articleData = new ArticleData();
        articleData.setTitle(article.getTitle());
        articleData.setIntro(article.getIntro());
        String[] split = article.getTags().split(",");
        int length = split.length;
        int[] ids = new int[length];
        for (int i = 0; i < length; i++) {
            ids[i] = Integer.parseInt(split[i]);
        }
        articleData.setTags_ids(ids);
        articleData.setBody(article.getBody());
        articleData.setState(article.getState());
        return articleData;
    }

    @Override
    public void delete(Integer id) {
        blogArticlesRepository.deleteById(id);
        commentsRepo.deleteAllByArticleId(id);
        updateNotUseTag(null, null, false);
    }

    @Override
    public List<IndexArticles> top(Integer pageNo, Integer pageSize) {
        List<IndexArticles> indexArticles = new ArrayList<>();
        Integer offset = (pageNo - 1) * pageSize;
        Integer rows = pageSize;
        List<ArticlesData> articlesData = commonMapper.selectArticlesNoBody(offset, rows);
        for (ArticlesData article : articlesData) {
            Timestamp createTime = article.getCreateTime();
            String format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(createTime);
            String shortTime = IntervalUtil.getShortTime(format);
            indexArticles.add(new IndexArticles(article.getId(), article.getTitle(), article.getReplies(), shortTime));
        }
        return indexArticles;
    }
}
