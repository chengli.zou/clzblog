package site.clzblog.admin.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import site.clzblog.entity.BlogTags;
import site.clzblog.admin.repo.BlogTagsRepository;
import site.clzblog.admin.service.BlogTagsService;
import site.clzblog.util.IntervalUtil;
import site.clzblog.vo.TagData;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class BlogTagsServiceImpl implements BlogTagsService {
    @Autowired
    private BlogTagsRepository blogTagsRepository;

    @Override
    public BlogTags save(String name, String status) {
        BlogTags blogTags = new BlogTags();
        blogTags.setName(name);
        blogTags.setStatus(status);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        blogTags.setCreateTime(timestamp);
        blogTags.setUpdateTime(timestamp);
        blogTags.setRemark("");
        return blogTagsRepository.save(blogTags);
    }

    @Override
    public List<TagData> findAll() {
        List<TagData> tagDataList = new ArrayList<>();
        List<BlogTags> all = blogTagsRepository.findAll();
        for (BlogTags tag : all) {
            String s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tag.getCreateTime());
            tagDataList.add(new TagData(tag.getId(), tag.getName(), tag.getStatus(), IntervalUtil.getShortTime(s)));
        }
        return tagDataList;
    }

    @Override
    public void deleteById(Integer id) {
        blogTagsRepository.deleteById(id);
    }

    @Override
    public void saveOrUpdate(Integer id, String name) {
        BlogTags blogTags;
        blogTags = blogTagsRepository.findById(id).get();
        blogTags.setName(name);
        blogTagsRepository.save(blogTags);
    }

    @Override
    public void saveOrUpdate(Integer id, String name, String status) {
        BlogTags blogTags;
        blogTags = blogTagsRepository.findById(id).get();
        if (null != name) {
            blogTags.setName(name);
        }
        if (null != status) {
            blogTags.setStatus(status);
        }
        blogTagsRepository.save(blogTags);
    }

    @Override
    public List<BlogTags> load() {
        return blogTagsRepository.findAll();
    }

    @Override
    public BlogTags findById(Integer id) {
        return blogTagsRepository.findById(id).get();
    }

    @Override
    public BlogTags findByName(String name) {
        return blogTagsRepository.findByName(name);
    }

    @Override
    public String find(Integer id) {
        return blogTagsRepository.findById(id).get().getName();
    }


}
