package site.clzblog.admin.service;

import site.clzblog.entity.BlogTags;
import site.clzblog.vo.TagData;

import java.util.List;

public interface BlogTagsService {
    BlogTags save(String name, String status);

    List<TagData> findAll();

    void deleteById(Integer id);

    void saveOrUpdate(Integer id, String name);

    void saveOrUpdate(Integer id, String name, String status);

    List<BlogTags> load();

    BlogTags findById(Integer id);

    BlogTags findByName(String name);

    String find(Integer id);
}
