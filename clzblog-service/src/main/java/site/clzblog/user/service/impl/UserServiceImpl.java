package site.clzblog.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import site.clzblog.entity.User;
import site.clzblog.mapper.SysUserMapper;
import site.clzblog.user.repo.UserRepository;
import site.clzblog.user.service.UserService;

import java.util.List;

/**
 * @Desc User service implement
 * @Author chengli.zou
 * @CreateDate 6/14/18
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> list() {
        return userRepository.findAll();
    }

    @Override
    public void updateStatusByName(String status, String name) {
        sysUserMapper.updateStatusByName(status, name);
    }

    @Override
    public String queryStatusByUsername(String username) {
        return sysUserMapper.findStatusByUsername(username);
    }

    @Override
    public void updateStatus(String status) {
        sysUserMapper.updateStatus(status);
    }
}
