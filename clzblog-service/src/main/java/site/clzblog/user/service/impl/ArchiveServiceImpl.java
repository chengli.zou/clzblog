package site.clzblog.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import site.clzblog.mapper.CommonMapper;
import site.clzblog.user.service.ArchiveService;
import site.clzblog.vo.PostsData;

import java.util.List;

/**
 * @Desc
 * @Author chengli.zou
 * @CreateDate 6/28/18
 */
@Service
public class ArchiveServiceImpl implements ArchiveService {
    @Autowired
    private CommonMapper commonMapper;

    @Override
    public List<Integer> years() {
        return commonMapper.postYears();
    }

    @Override
    public List<PostsData> year(Integer state, Integer year) {
        return commonMapper.selectArchiveByStateAndYear(state, year);
    }
}
