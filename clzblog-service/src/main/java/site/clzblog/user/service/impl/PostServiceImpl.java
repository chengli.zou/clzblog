package site.clzblog.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import site.clzblog.mapper.CommonMapper;
import site.clzblog.user.service.PostService;
import site.clzblog.vo.PostsData;

import java.util.List;

@Service
public class PostServiceImpl implements PostService {
    @Autowired
    private CommonMapper commonMapper;

    @Override
    public long count(Integer state) {
        return commonMapper.selectArticlesCountByState(state);
    }

    @Override
    public List<PostsData> page(Integer state, Integer pageNo, Integer pageSize) {
        Integer offset,rows;
        offset = (pageNo - 1) * pageSize;
        rows = pageSize;
        return commonMapper.selectPostsByState(state, offset, rows);
    }
}
