package site.clzblog.user.service;

import site.clzblog.vo.PostsData;

import java.util.List;

public interface PostService {
    long count(Integer state);

    List<PostsData> page(Integer state, Integer pageNo, Integer pageSize);
}
