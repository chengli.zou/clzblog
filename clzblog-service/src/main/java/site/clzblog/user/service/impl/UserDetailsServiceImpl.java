package site.clzblog.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import site.clzblog.entity.Roles;
import site.clzblog.entity.SysUser;
import site.clzblog.mapper.SysUserMapper;

import java.util.ArrayList;
import java.util.List;

/**
 * @Desc
 * @Author chengli.zou
 * @CreateDate 6/15/18
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        SysUser sysUser = sysUserMapper.findByName(s);

        if (sysUser == null) {
            throw new UsernameNotFoundException("User not exists");
        }

        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        for (Roles role : sysUser.getRoles()) {
            authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
            System.out.println(role.getRoleName());
        }

        return new User(sysUser.getUsername(), sysUser.getPassword(), authorities);
    }


}
