package site.clzblog.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import site.clzblog.entity.BlogComments;
import site.clzblog.entity.SysUser;
import site.clzblog.mapper.CommonMapper;
import site.clzblog.mapper.SysUserMapper;
import site.clzblog.user.repo.CommentsRepo;
import site.clzblog.user.service.CommentService;
import site.clzblog.vo.CommentData;

import java.sql.Timestamp;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    private CommentsRepo commentsRepo;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private CommonMapper commonMapper;

    @Override
    public boolean add(CommentData data) {
        BlogComments comment = new BlogComments();
        comment.setArticleId(data.getArticleId());
        comment.setType(null != data.getToUid() ? "CTC" : "CTA");
        comment.setFullName(data.getFullName());
        comment.setContent(data.getContent());
        comment.setFromUid(data.getFromUid());
        comment.setToUid(data.getToUid());
        comment.setToName(data.getToName());
        Integer floor = commonMapper.selectMaxFloorByArticleId(data.getArticleId());
        comment.setFloor(null != floor ? (floor + 1) : 1);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        comment.setCreateTime(timestamp);
        comment.setUpdateTime(timestamp);
        comment.setRemark("");
        BlogComments save = commentsRepo.save(comment);
        return 0 != save.getId() ? true : false;
    }

    @Override
    public Integer getCurrentUserId(String username) {
        return sysUserMapper.getCurrentUserId(username);
    }

    @Override
    public List<BlogComments> load(Long articleId) {
        return commentsRepo.findByArticleId(articleId);
    }

    @Override
    public SysUser getCurrentUser(String name) {
        return sysUserMapper.getCurrentUser(name);
    }

    @Override
    public void delete(Long id, long articleId, Integer floor) {
        commentsRepo.deleteById(id);
        List<BlogComments> list = commentsRepo.findByArticleIdAndFloorIsGreaterThan(articleId, floor);
        for (BlogComments comment: list) {
            comment.setFloor(comment.getFloor() - 1);
            commentsRepo.save(comment);
        }
    }
}
