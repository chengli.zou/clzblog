package site.clzblog.user.service;

import site.clzblog.entity.User;

import java.util.List;

/**
 * @Desc User service
 * @Author chengli.zou
 * @CreateDate 6/14/18
 */
public interface UserService {
    List<User> list();

    void updateStatusByName(String status, String name);

    String queryStatusByUsername(String username);

    void updateStatus(String status);
}
