package site.clzblog.user.service;

import site.clzblog.entity.BlogComments;
import site.clzblog.entity.SysUser;
import site.clzblog.vo.CommentData;

import java.util.List;

public interface CommentService {
    boolean add(CommentData data);

    Integer getCurrentUserId(String username);

    List<BlogComments> load(Long articleId);

    SysUser getCurrentUser(String name);

    void delete(Long id,long articleId, Integer floor);
}
