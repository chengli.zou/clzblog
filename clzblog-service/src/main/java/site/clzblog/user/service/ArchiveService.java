package site.clzblog.user.service;

import site.clzblog.vo.PostsData;

import java.util.List;

/**
 * @Desc
 * @Author chengli.zou
 * @CreateDate 6/28/18
 */
public interface ArchiveService {

    List<Integer> years();

    List<PostsData> year(Integer state, Integer year);
}
